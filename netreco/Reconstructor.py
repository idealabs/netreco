import time
from functools import partial
from multiprocessing import Pool

import numpy as np
import matplotlib.pyplot as plt

from scipy.sparse import csr_matrix
from scipy.sparse.linalg import lsqr
from scipy.optimize import differential_evolution

from sklearn import linear_model

from .ReconstructorLegacy import Profile

np.set_printoptions(precision=2, threshold=np.nan, linewidth=200,
                    suppress=True)


class Reconstructor:
    """Reconstructs a network from data.

    Available kwargs
    ----------------
    debug : bool, default=False
        True if debug or status messages should be displayed.
    r : int > 0
        The approximator for the convolution (smaller is faster, larger
        is more likely to reconstruct). If given equal or larger than f,
        will be thresholded to f-1 (where f is the number of data points
        passed into the algorithm's run function).
    order : number > 1, default=3
        w_k in the equations above. In other words, the number of poles
        to find in P and Q. There will be 2*order + 1 parameters in
        the fit function Q_ij(t) (a_k plus a b_i and a c_i for each i
        from 0 to order).
    bounds : number > 0, default=10
        All parameters b_i of the fit convolution Q_ij(t) (in other words,
        all a_k, b_i, and c_i) will be constrained to be between
        (-order,order). Note, all parameters c_i are assumed to be stable, or
        in other words, -1 < c_i < 1.
    robust : boolean, default=False
        True if the robust algorithm should be used (lasso least squares),
        False otherwise (regular least squares).
    T22 : numpy array (pm x l) or None, default=None
        The a-priory information known about the system (see the paper)
        for more information. Only used by the measured reconstruction.
    Pbool : numpy array (p x m) containing only 0's and 1's, or None.
            Default=None
        The known boolean structure of P. (Probably will only work if
        each row and each column has exactly one 1). Only used by the measured
        reconstruction.
    criterion : str ('bic' (default) or 'aic')
        The information criterion to do the robust (Lasso) least squares.
        bic is the Bayes Information Criterion and aic is the Akaike
        Information Criterion.
    njobs : int >= 1
        The number of jobs for multithreading, or 1 if no multithreading is to
        be used.
    build_P : bool, default=False
        If True, reconstructs Q and P, if false, reconstructs only Q.

    Sources
    -------
    [1] V. Chetty, J. Eliason and S. Warnick, "Passive Reconstruction of
        Non-Target-Specific Discrete-Time LTI Systems," American Control
        Conference, Boston, MA, 2016.
    """

    def __init__(self, **kwargs):
        self.debug = kwargs.get('debug', False)

        self.robust = kwargs.get('robust', False)
        self.alpha = kwargs.get('alpha', 1)
        self.T22 = kwargs.get('T22', None)
        self.Pbool = kwargs.get('Pbool', None)
        self.order = kwargs.get('order', 3)
        self.bounds = kwargs.get('bounds', 10)
        self.test_params = kwargs.get('test_params', False)
        self.criterion = kwargs.get('criterion', 'aic')  # or bic
        self.real_params = kwargs.get('real_params', {})
        self.njobs = kwargs.get('njobs', 1)
        self.build_P = kwargs.get('build_P', False)

        self.r = kwargs.get('r', 3)

        self.executed = False

    ###########################################################################
    #   Main Algorithms
    ###########################################################################

    def measured(self, y, u, ij=None, plot=False, titles={}):
        """Runs a network reconstruction for measured inputs. See [1] for the
        algorithm. Can use the robust or non-robust algorithm.

        Attempts to build the convolutional representation of the DSF given by

            y(t) = Q(t)*y(t) + P(t)*u(t)

        where * denotes the convolution operator. Q(t) is an (p x p) matrix
        of equations (see below), and P(t) is a (p x m) matrix of equations.

        Let n = max(t-1, r), where r <= f (f is the number of datapoints in the
        input/output set). This convolution is then given by

            y(1) = 0  AND
            y(t) = sum_{i = 1}^{n} [Q(r)y(n-i+1) + Pi(t)u(n-i+1)],  t > 1

        (Note, setting r < f creates an approximation of the convolution, which
        works for large enough r since, for stable systems, Q(r) = 0 as r
        approaches infinity).

        Each Q_ij(t) is fit to a
        function of the form

            Q_ij(t) = a_k delta_t,0 + sum_{i = 0}^{w_k} b_i(c_i)^t

        For parameter set a_k, b_i, and c_i (there will be 1 + 2*w_k
        parameters).

        Parameters
        ----------
        y : numpy array (f x p)
            The measured outputs.
        u : numpy array (f x m)
            The measured inputs.
        ij : 2-tuple or None, default=None
            If given, specifies a single entry in Q to fit a convolution to,
            otherwise all entries in Q will be fit. Note: 1-indexed. So
            ij = (1, 3) refers to Qis[0][2].
        plot : boolean, default=False
            If True, plots the reconstruction results.
        titles : dict {int => str}
            Labels for each output i (1-indexed)

        Returns
        -------
        params : dict of dict of list (size self.order * 2)
            Each param[i][j] is a list of the best-fit parameters for Q(i, j).
        """
        if self.executed:
            raise Exception('Already used a reconstruction on this object.'
                            'Please create a new reconstruction object.')

        beginning = time.time()

        T22 = self.prepare_measured(y, u)
        LQrows, LQcols, LQdata = self.build_LQ(y)
        LProws, LPcols, LPdata = self.build_LP(
            u, T22, LQrows, LQcols, LQdata
        )
        M = self.build_M(LProws, LPcols, LPdata, is_measured=True)

        yhat = self.build_yhat(y)
        xhat = self.run_lsqr(M, yhat)

        Qis = self.extract_Qis(xhat)
        params = self.fit_convolution(Qis, ij, plot, titles)

        if self.build_P:
            # TODO only works for diagonal P
            Pis = self.extract_Pis(xhat)
            pparams = self.fit_convolution(Pis, None, plot, {}, True)

        self.executed = True

        self.dprint('\nReconstruction Complete! Took {:.3f} Seconds\n'.format(
            time.time() - beginning
        ))

        if self.build_P:
            return [params, pparams]
        else:
            return params

    def unmeasured(self, y, ij=None, plot=False, titles={}):
        """Runs a network reconstruction for unmeasured inputs. See [1] for the
        algorithm. Must use the robust algorithm (forces it to True).

        Attempts to build the convolutional representation of the DSF given by

            y(t) = Q(t)*y(t) + Delta(t)

        where * denotes the convolution operator. Q(t) is an (p x p) matrix
        of equations (see below), and P(t) is a (p x m) matrix of equations.

        Let n = max(t-1, r), where r <= f (f is the number of datapoints in the
        input/output set). This convolution is then given by

            y(1) = 0  AND
            y(t) = sum_{i = 1}^{n} [Q(r)y(n-i+1) + Delta(t)],  t > 1

        (Note, setting r < f creates an approximation of the convolution, which
        works for large enough r since, for stable systems, Q(r) = 0 as r
        approaches infinity).

        Each Q_ij(t) is fit to a
        function of the form

            Q_ij(t) = a_k delta_t,0 + sum_{i = 0}^{w_k} b_i(c_i)^t

        For parameter set a_k, b_i, and c_i (there will be 1 + 2*w_k
        parameters).

        Parameters
        ----------
        y : numpy array (f x p)
            The measured outputs.
        ij : 2-tuple or None, default=None
            If given, specifies a single entry in Q to fit a convolution to,
            otherwise all entries in Q will be fit. Note: 1-indexed. So
            ij = (1, 3) refers to Qis[0][2].
        plot : boolean, default=False
            If True, plots the reconstruction results.
        titles : dict {int => str}
            Labels for each output i (1-indexed)

        Returns
        -------
        params : dict of dict of list (size self.order * 2)
            Each param[i][j] is a list of the best-fit parameters for Q(i, j).
        """
        if self.executed:
            raise Exception('Already used a reconstruction on this object.'
                            'Please create a new reconstruction object.')

        beginning = time.time()

        self.prepare_unmeasured(y)
        LQrows, LQcols, LQdata = self.build_LQ(y)
        M = self.build_M(LQrows, LQcols, LQdata, is_measured=False)

        yhat = self.build_yhat(y)
        xhat = self.run_lsqr(M, yhat)

        Qis = self.extract_Qis(xhat)
        params = self.fit_convolution(Qis, ij, plot, titles)

        self.executed = True

        self.dprint('\nReconstruction Complete! Took {:.3f} Seconds\n'.format(
            time.time() - beginning
        ))

        return params

    ###########################################################################
    #   Algorithm Steps
    ###########################################################################

    def prepare_measured(self, y, u):
        """Sets up a measured reconstruction.

        Parameters
        ----------
        y : numpy array (f x p)
            The measured outputs.
        u : numpy array (f x m)
        """
        with Profile(self.debug, 'Preparing for a Measured Reconstruction'):
            f, p = y.shape
            f1, m = u.shape

            self.r = min(self.r, f - 1)

            assert f == f1

            self.p = p    # Number of outputs
            self.m = m    # Number of inputs
            self.f = f    # Number of data points

            if self.Pbool is not None:
                p1, m1 = self.Pbool.shape
                assert p == p1
                assert m == m1
                self.T22 = self._build_T22(self.Pbool)
                ps = p1 * m1 - len(self.T22)
            elif self.T22 is not None:
                bogus, ps = self.T22.shape
                pass  # T22 already defined
            else:
                raise Exception('Not implemented - cannot reconstruct with '
                                'measured inputs without a Pbool.')

            self.ps = ps

        return self.T22

    def prepare_unmeasured(self, y):
        """Sets up a measured reconstruction.

        Parameters
        ----------
        y : numpy array (f x p)
            The measured outputs.
        """
        with Profile(self.debug, 'Preparing for an Unmeasured Reconstruction'):
            f, p = y.shape

            self.r = min(self.r, f - 1)

            self.p = p    # Number of outputs
            self.f = f    # Number of data points

            # self.robust = True

    def build_LQ(self, y):
        """
        Builds the Q half of L (see paper [1]).

        Parameters
        ----------
        y : numpy array (f x p)

        Returns
        -------
        LQ : numpy array (p(f-1) x r(p^2 - p))
            The Q half of L. Note that the paper is not entirely correct on the
            construction of L. It only goes to r(f-1) rows, using the first f-1
            points from y. Also, either L has r rows of zeros at the beginning
            (making rf rows) or yhat goes from y2 to yf. We choose the latter.
        """
        with Profile(self.debug, 'Building LQ'):
            width = self.p * self.p - self.p

            # Ignore all Q_ii
            to_ignore = [()]
            count = 0
            for i in range(self.p):
                for j in range(self.p):
                    if i == j:
                        to_ignore.append(count)
                    count += 1

            # Build the individual blocks
            Brows = [None for i in range(self.f - 1)]
            Bcols = [None for i in range(self.f - 1)]
            Bdats = [None for i in range(self.f - 1)]
            for i in range(self.f - 1):
                Brows[i], Bcols[i], Bdats[i] = \
                    self._build_block(y[i, :], to_ignore)

            # Build LQ
            rows = []
            cols = []
            data = []

            for i in range(self.f - 1):
                start_row = i * self.p
                for j in range(min(i + 1, self.r)):
                    start_col = j * width
                    block_index = i - j

                    rows.append(Brows[block_index] + start_row)
                    cols.append(Bcols[block_index] + start_col)
                    data.append(Bdats[block_index])

        return rows, cols, data

    def build_LP(self, u, T22, rows, cols, data, col_offset=None):
        """
        Builds the P half of L (see paper [1]).

        Parameters
        ----------
        u : numpy array (f x p)
        T22 : numpy array (mp x l)

        Returns
        -------
        LQ : numpy array (p(f-1) x rl))
            The P half of L. Note that the paper is not entirely correct on the
            construction of L. It only goes to r(f-1) rows, using the first f-1
            points from y. Also, either L has r rows of zeros at the beginning
            (making rf rows) or yhat goes from y2 to yf. We choose the latter.
        """
        with Profile(self.debug, 'Building LP'):

            if col_offset is None:
                col_offset = self.r * (self.p * self.p - self.p)

            l = self.m * self.p - len(T22)

            # Build the individual blocks
            Brows = [None for i in range(self.f - 1)]
            Bcols = [None for i in range(self.f - 1)]
            Bdats = [None for i in range(self.f - 1)]
            for i in range(self.f - 1):
                Brows[i], Bcols[i], Bdats[i] = self._build_block(u[i, :], T22)

            # Build LP
            for i in range(self.f - 1):
                start_row = i * self.p
                for j in range(min(i + 1, self.r)):
                    start_col = col_offset + j * l
                    block_index = i - j

                    rows.append(Brows[block_index] + start_row)
                    cols.append(Bcols[block_index] + start_col)
                    data.append(Bdats[block_index])

        return rows, cols, data

    def build_M(self, rows, cols, data, is_measured):
        """Builds the M sparse matrix from LQ (if unmeasured) or LP (if
        measured).

        Parameters
        ----------
        rows : list of lists
            The row indices of non-zero entries of LQ or LP.
        cols : list of lists
            The col indices of non-zero entries of LQ or LP. 1-1 with rows.
        data : list of lists
            The non-zero entry values of LQ or LP. 1-1 with rows and cols
            (meaning M[rows[i], cols[i]] = data[i]).

        Returns
        -------
        M : csr_matrix
        """
        with Profile(self.debug, 'Building M'):
            height = (self.f - 1) * self.p
            if is_measured:
                width = self.r * (self.ps + self.p * self.p - self.p)
            else:
                width = self.r * (self.p * self.p - self.p)

            M = csr_matrix((np.concatenate(data),
                            (np.concatenate(rows), np.concatenate(cols))),
                           (height, width))

        return M

    def build_yhat(self, y):
        """
        Stacks y into [y2' ... y2' ... yf' ... yf']''

        Parameters
        ----------
        y : numpy array (f x p)

        Returns
        -------
        yhat : numpy array (p(f-1) x 1)
        """
        with Profile(self.debug, 'Building yhat'):
            ybar = y[1:, :]
            yhat = ybar.reshape(self.p * (self.f - 1), 1)

        return yhat

    def run_lsqr(self, M, yhat):
        """
        Finds the xhat that "best" fits yhat = M*xhat.

        If the robust algorithm is not running, this is regular least squares,
        or in other words:

            xhat = argmin_x || yhat - M*xhat ||_2

        If the robust algorithm is running, this is the lasso relaxation of
        the sparse least squares, or in other words:

            xhat = argmin_x || yhat - M*xhat ||_2 + alpha * || xhat ||_1

        where alpha is set when the algorithm is called.
        """
        with Profile(self.debug, 'Running Least Squares to get xhat'):
            if self.robust:
                model = linear_model.LassoLarsIC(criterion=self.criterion)
                y = list(yhat.reshape(max(yhat.shape)))
                model.fit(M.toarray(), y)

                self.dprint(
                    '\tFinished in {} iterations'.format(model.n_iter_)
                )
                xhat = np.array(model.coef_).reshape(
                    (len(model.coef_), 1)
                )

                xhat = np.array(model.coef_).reshape(
                    (len(model.coef_), 1)
                )

            else:
                x, istop, itn, r1norm, r2norm, arnorm, acond, arnorm, xnorm, \
                    var = lsqr(M, yhat)
                xhat = np.array(x).reshape((len(x), 1))
                self.dprint('\tFinished lsq: Error = {:.3f}'.format(r1norm))

        return xhat

    def extract_Qis(self, xhat):
        """
        Extracts the Qi's from the given xhat vector.
        """
        with Profile(self.debug, 'Extracting Qis'):
            Qis = {}
            width = self.p * self.p - self.p

            for i in range(self.r):
                vec = xhat[i * width: i * width + width]

                pos = 0
                for j in range(self.p):

                    if j not in Qis:
                        Qis[j] = {}

                    for k in range(self.p):
                        if j == k:
                            continue

                        curr = Qis[j].get(k, [0])
                        curr.append(vec[pos][0])
                        Qis[j][k] = curr

                        pos += 1

        return Qis

    def extract_Pis(self, xhat):
        """
        Extracts the Pi's from the given xhat vector.
        """
        # TODO: This only works if p is diagonal

        offset = self.r * (self.p * self.p - self.p)
        with Profile(self.debug, 'Extracting Pis'):
            Pis = {}
            width = self.p

            for i in range(self.r):
                vec = xhat[i * width + offset: i * width + width + offset]

                for j in range(self.p):

                    if j not in Pis:
                        Pis[j] = {}

                    curr = Pis[j].get(j, [0])
                    curr.append(vec[j][0])
                    Pis[j] = {}
                    Pis[j][j] = curr

        return Pis

    def fit_convolution(self, Qis, ij, plot, titles, is_P=False):
        """Fits the convolutional representation to Q.
        """

        if ij is not None:
            i = ij[0]
            j = ij[1]
            sub = self._fit_ij_conv(Qis, i - 1, j - 1, plot, titles)

            params = {i: {j: sub}}
            return params
        elif self.njobs > 1:
            rs = _setup_jobs(self.njobs, self.p, Qis, plot, titles, self, is_P)

            params = {}
            for r in rs:
                i = r[0]
                j = r[1]
                param = r[2]
                sub = params.get(i, {})
                sub[j] = param
                params[i] = sub

            return params

        else:
            params = {}
            for i in Qis.keys():
                params[i] = {}
                for j in Qis[i].keys():
                    params[i][j] = self._fit_ij_conv(Qis, i, j, plot, titles,
                                                     is_P)

            return params

    def params2series(self, Qparams, Pparams, T=None):
        """Converts the parameters representing the convolutional form of
        (Q, P) to a time series [Q1, Q2, ...], etc.

        Parameters
        ----------
        Qparams : dict of dict of list of number
            Qparams[i][j] contains the parameters of the convolutional form of
            link Q_ij as returned by `fit_convolution`. The dictionary is
            sparse, meaning Qparams[1][1], etc. may not exist.
        Pparams : dict of dict of list of number
            Pparams[i][j] contains the parameters of the convolutional form of
            link P_ij as returned by `fit_convolution`, The dictionary is
            sparse, meaning Pparams[1][2], etc. may not exist.
        T : int > 0, defaults to number of data points given in input/output
            The length of the time series to generate.


        Returns
        -------
        Qseries : dict of dict of list of number of length T
            Qseries[i][j] contains the time series representation of link Q_ij,
            meaning Qseries[i][j] = [q_ij[0], q_ij[1], ..., q_ij[T - 1]].
            Qseries has the same sparsity pattern as Qparams.
        Pseries : dict of dict of list of number of length T
            Pseries[i][j] contains the time series representation of link P_ij,
            meaning Pseries[i][j] = [p_ij[0], p_ij[1], ..., p_ij[T - 1]].
            Pseries has the same sparsity pattern as Pparams.
        """
        if T is None:
            T = self.f

        Qseries = {}
        for i, row in Qparams.items():
            Qseries[i] = {}
            for j, row in Qparams[i].items():
                err, exp = self._fiterr(Qparams[i][j], np.zeros(T), True)
                Qseries[i][j] = exp

        Pseries = {}
        for i, row in Pparams.items():
            Pseries[i] = {}
            for j, row in Pparams[i].items():
                err, exp = self._fiterr(Pparams[i][j], np.zeros(T), True)
                Pseries[i][j] = exp

        return Qseries, Pseries

    def simulate(self, Qseries, Pseries, u, r=None):
        """Simulates y = Qy + Pu for T time steps.

        Parameters
        ----------
        Qseries : dict of dict of list of number of length T
            As returned by `params2series`.
        Pseries : dict of dict of list of number of length T
            As returned by `param2series`.
        u : numpy.array (T x m)
            The inputs on which to simulate.
        r : int > 0, default=self.r
            The length of the Q, P series to use in the convolution.

        Returns
        -------
        yest : numpy.array (T x p)
            The simulated outputs.
        """
        if r is None:
            r = self.r

        T, m = u.shape
        assert m == self.m
        p = self.p

        yest = np.zeros((T, p))
        for t in range(1, T):
            for i in range(p):
                yi_curr = 0
                for j in range(p):
                    if i in Qseries and j in Qseries[i]:
                        Qij = Qseries[i][j][:t+1]
                        s = t + 1 - len(Qij)
                        yi_curr += np.sum(
                            Qij * np.flip(yest[s:t+1, j], axis=0)
                        )
                for j in range(m):
                    if i in Pseries and j in Pseries[i]:
                        Pij = Pseries[i][j][:t+1]
                        s = t + 1 - len(Pij)
                        yi_curr += np.sum(
                            Pij * np.flip(u[s:t+1, j], axis=0)
                        )
                yest[t, i] = yi_curr

        return yest

    ###########################################################################
    #   Helpers
    ###########################################################################

    def _fit_ij_conv(self, Qis, i, j, plot, titles, is_P=False):
        """Fits the convolutional representation to Q(i,j)
        """
        sidename = {False: 'Q', True: 'P'}[is_P]
        with Profile(self.debug, 'Learning Convolution for {}({},{})'.format(
            sidename, i + 1, j + 1
        )):

            t = list(range(self.r + 1))
            vals = Qis[i][j]

            bounds = []
            stab_tol = 0.001  # Prevent the system from being marginally stable
            for k in range(self.order):
                bounds.append((-self.bounds, self.bounds))
                bounds.append((-1 + stab_tol, 1 - stab_tol))

            assert len(t) == len(vals)

            obj = partial(self._fiterr, act=vals)
            rs = differential_evolution(obj, bounds=bounds)

            if plot:
                err, exp = self._fiterr(rs.x, vals, True)

                plt.figure(figsize=(10, 8))
                plt.scatter(t, vals, c='r', label='xhat')
                plt.plot(t, exp, c='b',
                         label='Reconstructed Convolution Model')

                if (i + 1, j + 1) in self.real_params:
                    rp = self.real_params[(i + 1, j + 1)]
                    err1, real = self._fiterr(rp, vals, True)
                    plt.plot(t, real, color='g', linestyle='-.',
                             label='Actual Convolution Model')

                plt.xlim((0, self.r))
                plt.xlabel('t')

                linkname = ''
                if (i + 1) in titles and (j + 1) in titles:
                    linkname = ': {} <-- {}'.format(titles[i + 1],
                                                    titles[j + 1])

                plt.title('{}(t)[{},{}]{}'.format(
                    sidename, i + 1, j + 1, linkname
                ))
                plt.legend()

            self._print_dsf(i, j, rs.x, t, vals, titles, is_P)

        return rs.x

    def _fiterr(self, params, act, return_exp=False):
        """Computes the error of a convolutional representation parameterized
        by params to the data act.
        """
        n = len(act)
        exp = np.zeros(n)
        for t in range(1, n):
            for i in range(int(len(params) / 2)):
                exp[t] += params[i * 2] * params[i * 2 + 1] ** t

        sqerr = (np.array(act) - exp) ** 2
        rmse = np.sqrt(sqerr.mean())
        # abserr = abs(np.array(act) - exp)
        # rmse = abserr.mean()

        # print(toterr)

        if return_exp:
            return rmse, exp
        else:
            return rmse

    def _build_T22(self, PBool):
        """
        Takes a boolean structure of P and cconverts it into a T22.
        """
        to_delete = []

        index = 0
        for i in range(self.p):
            for j in range(self.m):
                if PBool[i, j] == 0:
                    to_delete.append(index)

                index += 1

        return to_delete

    def _build_block(self, v, to_ignore=[]):
        """
        Builds the diagonal portion of the block used in both LQ and LP, which
        is [v' 0 ... 0; 0 v' ... 0 ; ... ; 0 0 ... v'], where the number of
        rows is p.

        The block is actually just a dictionary of {(row, col) => value}

        Parameters
        ----------
        v : numpy array (length l = length p or length m or arbitrary)
            The data belonging in the block. It is either a vector from u or
            from y.
        to_ignore : list (len <= p*l)
            Columns to remove from the block, where the index is referring
            to one of the p*l columns, not the l entries of v.

        Returns
        -------
        row : np.array (len = p*l - len(to_ignore))
            The row indices of the non-zero data
        col : np.array (len = p*l - len(to_ignore))
            The column indices of the non-zero data, corresponding to row
        data : np.array (len = p*l - len(to_ignore))
            The non-zero entries, where data[i] is located at index
            (row[i], col[i])
        """
        vt = v.transpose().tolist()
        l = len(vt)

        row = []
        col = []
        data = []
        loc = 0
        for i in range(self.p):
            for j in range(l):
                pos = i * l + j
                if pos in to_ignore:
                    continue

                row.append(i)
                col.append(loc)
                data.append(vt[j])
                loc += 1

        return np.array(row), np.array(col), np.array(data)

    def _print_dsf(self, i, j, params, t, data, titles, is_P=False):
        if not self.debug:
            return

        linkname = ''
        if (i + 1) in titles and (j + 1) in titles:
            linkname = ' {} <-- {}'.format(titles[i + 1], titles[j + 1])

        sidename = {False: 'Q', True: 'P'}[is_P]
        msg = '\t{}({},{}){}: '.format(sidename, i + 1, j + 1, linkname)
        poles = []

        a = 0
        for i in range(self.order):
            b = i * 2
            c = b + 1
            poles.append('{:.3f}*({:.3f})^t'.format(params[b], params[c]))
            a += -params[b]

        msg += ' + '.join(poles)
        msg += ' + {:3f} * delta(t,0)'.format(a)
        print(msg)

        err = self._fiterr(params, data)
        # actual = np.array(data)

        # errors = actual - expected
        # rmse = np.sqrt((errors ** 2).mean())
        print('\t\tRMSE = {:.3f}'.format(err))

        print('\t\tMatlab: {}'.format(params))

    def dprint(self, msg):
        """
        Prints the debug msg `msg` conditional on debug being set to True.
        """
        if self.debug:
            print(msg)


###############################################################################
#   Multiprocessing
###############################################################################

def _setup_jobs(njobs, p, Qis, plot, titles, recon, is_P=False):
    pool = Pool(njobs)
    ijs = []

    to_run = partial(_run_job, Qis=Qis, plot=plot, titles=titles, recon=recon,
                     is_P=is_P)

    for i in Qis.keys():
        for j in Qis[i].keys():
            ijs.append((i, j))

    rs = pool.map(to_run, ijs)
    return rs


def _run_job(ij, Qis, plot, titles, recon, is_P=False):
    i = ij[0]
    j = ij[1]
    return (i, j, recon._fit_ij_conv(Qis, i, j, plot, titles, is_P))
