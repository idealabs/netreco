import numpy as np


def ss_sim(A, B, C, u, D=None):
    """
    Simulates the discrete time system (assuming x[0] = 0):

        x[k+1] = Ax[k] + Bu[k]
        y[k] = Cx[k] {+ Du[k]}

    Parameters
    ----------
    A : numpy array (n x n)
    B : numpy array (n x m)
    C : numpy array (p x n)
    u : numpy array (f x m)
        Row i is u[i]'
    D : numpy array (p x m), default=0
        An optional D matrix

    Returns
    -------
    y : numpy array (f x p)
        Row i is y[i]'
    """
    n, n1 = A.shape
    n2, m = B.shape
    p, n3 = C.shape
    f, m1 = u.shape

    assert n == n1
    assert n == n2
    assert n == n3
    assert m == m1

    if D is None:
        D = np.zeros((p, m))

    xi = np.zeros((n, 1))[:, 0]
    y = np.zeros((f, p))

    for i in range(f):
        y[i] = C.dot(xi) + D.dot(u[i])
        xi = A.dot(xi) + B.dot(u[i])

    return y
