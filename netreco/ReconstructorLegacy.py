import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import lsqr
from scipy.optimize import curve_fit, OptimizeWarning
from sklearn import linear_model
import time
import warnings

np.set_printoptions(precision=2, threshold=np.nan, linewidth=200,
                    suppress=True)


class Reconstructor:
    """Reconstructs a network from data.

    Parameters
    ----------
    debug : bool, default=False
        True if debug or status messages should be displayed.

    Sources
    -------
    [1] V. Chetty, J. Eliason and S. Warnick, "Passive Reconstruction of
        Non-Target-Specific Discrete-Time LTI Systems," American Control
        Conference, Boston, MA, 2016.
    """

    def __init__(self, debug=False):
        self.debug = debug

    def measured(self, y, u, r, **kwargs):
                # order=2, robust=False, alpha=1, T22=None,
                #  Pbool=None):
        """Runs a network reconstruction for measured inputs. See [1] for the
        algorithm.

        Attempts to build the convolutional representation of the DSF given by

            y(t) = Q(t)*y(t) + P(t)*u(t)

        where * denotes the convolution operator. Q(t) is an (p x p) matrix
        of equations (see below), and P(t) is a (p x m) matrix of equations.

        Let n = max(t-1, r), where r <= f (f is the number of datapoints in the
        input/output set). This convolution is then given by

            y(1) = 0  AND
            y(t) = sum_{i = 1}^{n} [Q(r)y(n-i+1) + Pi(t)u(n-i+1)],  t > 1

        (Note, setting r < f creates an approximation of the convolution, which
        works for large enough r since, for stable systems, Q(r) = 0 as r
        approaches infinity).

        Each Q_ij(t) is fit to a
        function of the form

            Q_ij(t) = a_k delta_t,0 + sum_{i = 0}^{w_k} b_i(c_i)^t

        For parameter set a_k, b_i, and c_i (there will be 1 + 2*w_k
        parameters).


        Parameters
        ----------
        y : numpy array (f x p)
            The measured outputs.
        u : numpy array (f x m)
            The measured inputs.
        r : number < f
            The approximator for the convolution (smaller is faster, larger
            is more likely to reconstruct). If given equal or larger than f,
            will be thresholded to f-1.
        order : number > 1, default=3
            w_k in the equations above. In other words, the number of poles
            to find in P and Q. There will be 2*order + 1 parameters in
            the fit function Q_ij(t) (a_k plus a b_i and a c_i for each i
            from 0 to order).
        bounds : number > 0, default=10
            All parameters of the fit convolution Q_ij(t) (in other words,
            all a_k, b_i, and c_i) will be constrained to be between
            (-order,order)
        max_iterations : int > 0, default=10
            The maximum iterations for the nonlinear least squares to attempt
            to fit Q_ij(t). An exception is raised if the fit doesn't occur
            within these limits.
        test_params : boolean, default=False
            True if the test parameters should be used. If True, order and
            bounds are ignored, and max_iterations is set to 1.
        robust : boolean, default=False
            True if the robust algorithm should be used (lasso least squares),
            False otherwise (regular least squares).
        alpha : number > 0, default=1
            The parameter weighting the desire for sparseness. See
            `self._run_lsq()` for more information.
        T22 : numpy array (pm x l) or None, default=None
            The a-priory information known about the system (see the paper)
            for more information. If not given, Pbool must be given.
        Pbool : numpy array (p x m) containing only 0's and 1's, or None.
                Default=None
            The known boolean structure of P. (Probably will only work if
            each row and each column has exactly one 1).
        """
        beginning = time.time()

        with Profile(self.debug, 'Setting up'):
            self.robust = kwargs.get('robust', False)
            self.alpha = kwargs.get('alpha', 1)
            self.T22 = kwargs.get('T22', None)
            self.Pbool = kwargs.get('Pbool', None)
            self.order = kwargs.get('order', 3)
            self.bounds = kwargs.get('bounds', 10)
            self.test_params = kwargs.get('test_params', False)

            if self.test_params:
                self.max_iterations = 1
            else:
                self.max_iterations = kwargs.get('max_iterations', 10)

            f, p = y.shape
            f1, m = u.shape

            assert f == f1
            assert not (self.Pbool is None and self.T22 is None)

            self.p = p
            self.m = m
            self.f = f

            r = min(r, f - 1)
            self.r = r

            if self.T22 is None:
                p1, m1 = self.Pbool.shape
                assert p == p1
                assert m == m1
                self.T22 = self._build_T22(self.Pbool)
                ps = p1 * m1 - len(self.T22)
            else:
                pm, l = self.T22.shape
                assert p*m == pm
                # Need to convert to to ignore list
                raise Exception('Not implemented')

        with Profile(self.debug, 'Building LQ'):
            rows, cols, data = self._build_LQ(y)
            # LQ = csr_matrix((np.concatenate(data),
            #                  (np.concatenate(rows),
            #                   np.concatenate(cols))
            #                  ),
            #                 ((self.f - 1) * self.p,
            #                  self.r * (self.p * self.p - self.p)))
            # print(LQ.shape)
            # print(LQ.toarray())

        with Profile(self.debug, 'Building LP'):
            # rows = []
            # cols = []
            # data = []
            # col_offset = 0
            col_offset = self.r * (self.p * self.p - self.p)
            rows, cols, data = self._build_LP(u, self.T22, rows, cols, data,
                                              col_offset)
            # LP = csr_matrix((np.concatenate(data),
            #                   (np.concatenate(rows),
            #                    np.concatenate(cols))
            #                   ),
            #                  ((self.f - 1) * self.p,
            #                   self.r * ps))
            # print(LP.shape)
            # print(LP.toarray())

        with Profile(self.debug, 'Building M'):
            M = csr_matrix((np.concatenate(data),
                            (np.concatenate(rows),
                             np.concatenate(cols))
                            ),
                           ((self.f - 1) * self.p,
                            self.r * (ps + self.p * self.p - self.p)))
            # print(M.toarray())

        with Profile(self.debug, 'Building yhat'):
            yhat = self._build_yhat(y)

        with Profile(self.debug, 'Running Least Squares for yhat = M*xhat'):
            xhat = self._run_lsq(M, yhat)

        with Profile(self.debug, 'Building Qi\'s'):
            Qis = self._extract_Qis(xhat)

        for i in range(self.p):
            for j in range(self.p):
                self._fit_conv(Qis, i, j)

        self.dprint('\nReconstruction Complete! Took {:.3f} Seconds\n'.format(
            time.time() - beginning
        ))

    ###########################################################################
    #   Helpers
    ###########################################################################

    def dprint(self, msg):
        """
        Prints the debug msg `msg` conditional on debug being set to True.
        """
        if self.debug:
            print(msg)

    def _build_T22(self, PBool):
        """
        Takes a boolean structure of P and cconverts it into a T22.
        """
        to_delete = []

        index = 0
        for i in range(self.p):
            for j in range(self.m):
                if PBool[i, j] == 0:
                    to_delete.append(index)

                index += 1

        return to_delete

    def _build_LQ(self, y):
        """
        Builds the Q half of L (see paper [1]).

        Parameters
        ----------
        y : numpy array (f x p)

        Returns
        -------
        LQ : numpy array (p(f-1) x r(p^2 - p))
            The Q half of L. Note that the paper is not entirely correct on the
            construction of L. It only goes to r(f-1) rows, using the first f-1
            points from y. Also, either L has r rows of zeros at the beginning
            (making rf rows) or yhat goes from y2 to yf. We choose the latter.
        """
        width = self.p * self.p - self.p

        # Ignore all Q_ii
        to_ignore = [()]
        count = 0
        for i in range(self.p):
            for j in range(self.p):
                if i == j:
                    to_ignore.append(count)
                count += 1

        # Build the individual blocks
        Brows = [None for i in range(self.f - 1)]
        Bcols = [None for i in range(self.f - 1)]
        Bdats = [None for i in range(self.f - 1)]
        for i in range(self.f - 1):
            Brows[i], Bcols[i], Bdats[i] = \
                self._build_block(y[i, :], to_ignore)

        # Build LQ
        rows = []
        cols = []
        data = []

        for i in range(self.f - 1):
            start_row = i * self.p
            for j in range(min(i + 1, self.r)):
                start_col = j * width
                block_index = i - j

                rows.append(Brows[block_index] + start_row)
                cols.append(Bcols[block_index] + start_col)
                data.append(Bdats[block_index])

        return rows, cols, data

    def _build_LP(self, u, T22, rows, cols, data, col_offset):
        """
        Builds the P half of L (see paper [1]).

        Parameters
        ----------
        u : numpy array (f x p)
        T22 : numpy array (mp x l)

        Returns
        -------
        LQ : numpy array (p(f-1) x rl))
            The P half of L. Note that the paper is not entirely correct on the
            construction of L. It only goes to r(f-1) rows, using the first f-1
            points from y. Also, either L has r rows of zeros at the beginning
            (making rf rows) or yhat goes from y2 to yf. We choose the latter.
        """
        l = self.m * self.p - len(T22)

        # Build the individual blocks
        Brows = [None for i in range(self.f - 1)]
        Bcols = [None for i in range(self.f - 1)]
        Bdats = [None for i in range(self.f - 1)]
        for i in range(self.f - 1):
            Brows[i], Bcols[i], Bdats[i] = self._build_block(u[i, :], T22)

        # Build LP
        for i in range(self.f - 1):
            start_row = i * self.p
            for j in range(min(i + 1, self.r)):
                start_col = col_offset + j * l
                block_index = i - j

                rows.append(Brows[block_index] + start_row)
                cols.append(Bcols[block_index] + start_col)
                data.append(Bdats[block_index])

        return rows, cols, data

    def _build_block(self, v, to_ignore=[]):
        """
        Builds the diagonal portion of the block used in both LQ and LP, which
        is [v' 0 ... 0; 0 v' ... 0 ; ... ; 0 0 ... v'], where the number of
        rows is p.

        The block is actually just a dictionary of {(row, col) => value}

        Parameters
        ----------
        v : numpy array (length l = length p or length m or arbitrary)
            The data belonging in the block. It is either a vector from u or
            from y.
        to_ignore : list (len <= p*l)
            Columns to remove from the block, where the index is referring
            to one of the p*l columns, not the l entries of v.

        Returns
        -------
        row : np.array (len = p*l - len(to_ignore))
            The row indices of the non-zero data
        col : np.array (len = p*l - len(to_ignore))
            The column indices of the non-zero data, corresponding to row
        data : np.array (len = p*l - len(to_ignore))
            The non-zero entries, where data[i] is located at index
            (row[i], col[i])
        """
        vt = v.transpose().tolist()
        l = len(vt)

        row = []
        col = []
        data = []
        loc = 0
        for i in range(self.p):
            for j in range(l):
                pos = i * l + j
                if pos in to_ignore:
                    continue

                row.append(i)
                col.append(loc)
                data.append(vt[j])
                loc += 1

        return np.array(row), np.array(col), np.array(data)

    def _build_yhat(self, y):
        """
        Stacks y into [y2' ... y2' ... yf' ... yf']''

        Parameters
        ----------
        y : numpy array (f x p)

        Returns
        -------
        yhat : numpy array (p(f-1) x 1)
        """
        ybar = y[1:, :]
        yhat = ybar.reshape(self.p * (self.f - 1), 1)

        return yhat

    def _run_lsq(self, M, yhat):
        """
        Finds the xhat that "best" fits yhat = M*xhat.

        If the robust algorithm is not running, this is regular least squares,
        or in other words:

            xhat = argmin_x || yhat - M*xhat ||_2

        If the robust algorithm is running, this is the lasso relaxation of
        the sparse least squares, or in other words:

            xhat = argmin_x || yhat - M*xhat ||_2 + alpha * || xhat ||_1

        where alpha is set when the algorithm is called.
        """
        if self.robust:
            model = linear_model.LassoLarsIC(criterion=self.criterion)
            y = list(yhat.reshape(max(yhat.shape)))
            model.fit(M.toarray(), y)

            self.dprint('\tFinished in {} iterations'.format(model.n_iter_))
            xhat = np.array(model.coef_).reshape(
                (len(model.coef_), 1)
            )

            xhat = np.array(model.coef_).reshape(
                (len(model.coef_), 1)
            )

        else:
            x, istop, itn, r1norm, r2norm, arnorm, acond, arnorm, xnorm, \
                var = lsqr(M, yhat)
            xhat = np.array(x).reshape((len(x), 1))
            self.dprint('\tFinished lsq: {}'.format(istop))

        return xhat

    def _extract_Qis(self, xhat):
        """
        Extracts the Qi's from the given xhat vector.
        """
        Qis = {}
        width = self.p * self.p - self.p

        for i in range(self.r):
            vec = xhat[i * width: i * width + width]

            pos = 0
            for j in range(self.p):

                if j not in Qis:
                    Qis[j] = {}

                for k in range(self.p):
                    if j == k:
                        continue

                    curr = Qis[j].get(k, [0])
                    curr.append(vec[pos][0])
                    Qis[j][k] = curr

                    pos += 1

        return Qis

    def _fit_conv(self, Qis, i, j):
        """
        Runs the fit for the convolution.
        """
        if i == j:
            return
        if i != 0 or j != 2:
            return

        t = list(range(1, self.r + 1))
        vals = Qis[i][j][1:]
        assert len(t) == len(vals)

        with Profile(self.debug, 'Learning Convolution for Q({},{})'.format(
            i + 1, j + 1
        )):

            sentinal = 0
            found = False
            rs = {}

            found_count = 0
            for sentinal in range(self.max_iterations):
                warnings.simplefilter('error')
                try:
                    bounds = None
                    if self.test_params:
                        p0 = [1, 1, -1, -1, -1, 1]
                        bounds = (-1, 1)
                    else:
                        p0 = np.random.rand(self.order * 2) \
                            * 2 * self.bounds - self.bounds
                        for k in range(self.order):
                            # c's are always between -1 and 1
                            p0[k * 2 + 1] /= self.bounds

                        bounds = ([], [])
                        for k, val in enumerate(p0):
                            if k % 2 == 1:
                                p0[k] = val / self.bounds
                                bounds[0].append(-1)
                                bounds[1].append(1)
                            else:
                                bounds[0].append(-self.bounds)
                                bounds[1].append(self.bounds)

                    popt, pcov = curve_fit(
                        fdsf, t, vals, p0=p0,
                        bounds=bounds
                    )
                    # self._print_dsf(i, j, list(popt), t, Qis[i][j], pcov)
                    found = True

                    rs[sentinal] = dict(popt=popt, pcov=pcov)
                    found_count += 1

                except (Exception, OptimizeWarning, Warning) as e:
                    found = False
                    e
                    # print(e)

                sentinal += 1

            if not found:
                raise Exception((
                                    'Could not find Q_{},{} in {} iterations'
                                ).format(i + 1, j + 1, self.max_iterations))

            mn = min(rs.keys(),
                     key=(lambda key: rs[key]['pcov'].max()))

            best = rs[mn]
            print('\tFound {}/{} possible fits, best below'.format(
                found_count, self.max_iterations
            ))
            self._print_dsf(i, j, list(best['popt']), t, vals,
                            best['pcov'])

            # print([(key, item['pcov'].max()) for key, item in rs.items()])

    def _print_dsf(self, i, j, params, t, data, cov):
        if not self.debug:
            return
        msg = '\tQ({},{}): '.format(i + 1, j + 1)
        poles = []

        a = 0
        for i in range(self.order):
            b = i * 2
            c = b + 1
            poles.append('{:.3f}*({:.3f})^t'.format(params[b], params[c]))
            a += -params[b]

        msg += ' + '.join(poles)
        msg += ' + {:3f} * delta(t,0)'.format(a)
        print(msg)

        expected = np.array(fdsf(t, *params))
        actual = np.array(data)

        errors = actual - expected
        rmse = np.sqrt((errors ** 2).mean())
        print('\t\tRMSE = {:.3f}, Max Covariance = {:.3f}'.format(
            rmse, cov.max()
        ))


def fdsf(t, *params):
    """
    Convolutional representation to fit.
    """
    poles = len(params) / 2
    assert poles % 1 == 0
    poles = int(poles)

    y = []
    for i in t:
        yi = 0
        for j in range(poles):
            b = j * 2
            c = b + 1
            yi += params[b] * params[c] ** i

        y.append(yi)

    return y


def kron_d(a, b):
    if a == b:
        return 1
    else:
        return 0


class Profile:
    """
    Used for Profiling.
    """

    def __init__(self, debug, msg):
        self.debug = debug
        self.msg = msg

    def __enter__(self):
        self.dstart(self.msg)

    def __exit__(self, type, value, traceback):
        self.dend()

    def dprint(self, msg):
        """
        Prints the debug msg `msg` conditional on debug being set to True.
        """
        if self.debug:
            print(msg)

    def dstart(self, task_name):
        """
        Starts a debugging profile with a message.
        """
        assert not hasattr(self, 'start') or self.start is None
        self.start = time.time()
        self.dprint('{} ...'.format(task_name))

    def dend(self):
        """
        Ends the last profiling task.
        """
        assert hasattr(self, 'start')
        assert self.start is not None
        self.dprint('\tDone. Took {:.3f} Seconds'.format(
            time.time() - self.start)
        )
        self.start = None
