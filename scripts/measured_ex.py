import numpy as np
import pandas as pd

from netreco import Reconstructor, ss_sim

R = 3


if __name__ == '__main__':
    A = [
        [0.75, 0, 0, 0, 0, 1.2],
        [-.1, -.35, 0, 0, 0, 0],
        [0, 0, .85, -1, 0, 0],
        [0, -.73, 0, .95, 0, 0],
        [0, 0, .43, 0, -.6, 0],
        [0, 0, 0, 0, .2, .55]

    ]
    A = np.array(A)
    B = [
        [1.4, 0, 0],
        [0, -.25, 0],
        [0, 0, 0.75],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]
    B = np.array(B)
    C = [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0]
    ]
    C = np.array(C)

    u = pd.read_csv('u.csv', header=None)
    u = u.values
    y = ss_sim(A, B, C, u)

    Pbool = np.identity(3)

    recon = Reconstructor(debug=True)
    recon.measured(y, u, R, Pbool=Pbool, test_params=True)
